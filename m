#!/bin/bash

JOBS=$(($(nproc) - 1))

if [[ -f "build/Makefile" ]]; then
	pushd build >> /dev/null
	make -j$JOBS || exit $?
	popd >> /dev/null
elif [[ -f "Makefile" ]]; then
	make -j$JOBS || exit $?
else
	echo "Unable to find Makefile"
	exit 1
fi
