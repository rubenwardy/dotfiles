#!/bin/bash

source /usr/share/git/completion/git-prompt.sh

PROMPT_COMMAND=__prompt_command

__prompt_command() {
	local exit="$?"    # This needs to be first

	local COLOR_CLEAR="\[\e[m\]"
	local BG_GREEN="\[\e[42;30m\]"
	local BG_RED="\[\e[41m\]"
	local C_AUTHOR="\[\e[0;33m\]"
	local C_PATH="\[\e[0;32m\]"
	local C_BRANCH="\[\e[0;93m\]"

	if [ "$exit" != 0 ]; then
		PS1="$BG_RED $exit $COLOR_CLEAR "
	else
		PS1="$BG_GREEN $exit $COLOR_CLEAR "
	fi

	PS1+="$C_AUTHOR$(git config user.name):$COLOR_CLEAR"
	PS1+="$C_PATH\w$COLOR_CLEAR"
	PS1+="$C_BRANCH$(__git_ps1 " (%s)") $COLOR_CLEAR\$ "

	# Set the terminal title
	printf "\033]0;%s\007" "`basename $PWD`"
}

source /usr/share/undistract-me/long-running.bash
notify_when_long_running_commands_finish_install

alias isa="cd ~/dev/llvm-project/llvm/lib/ISAExtend"
alias mt="cd ~/dev/minetest"
alias jkl="bundle exec jekyll serve --drafts --unpublished --livereload"
alias jks="bundle exec jekyll serve --drafts --unpublished --livereload"
alias dtf="cd ~/dev/dotfiles"
alias rvwp="cd ~/dev/rvwp"
alias dev="cd ~/dev"
alias llvm="cd ~/dev/llvm-project"
alias pyv="source env/bin/activate"
alias dotfiles="cd ~/dev/dotfiles/"
alias gsm="git submodule add"
alias hiperf="sudo cpupower frequency-set -g performance"
alias loperf="sudo cpupower frequency-set -g powersave"
alias fixfox="rm -rf ~/.cache/mozilla/firefox/1fcmj42i.default-release"
alias ll="ls -lah"
alias alarm-music="xdg-open '/home/ruben/Music/The Neighbourhood/I Love You/01-the_neighbourhood-how.mp3'"
alias alarm="sudo rtcwake -m mem -l -t "$(date +%s -d 'today 08:30')"; alarm-music"
alias alarm-test="sudo rtcwake -m mem -s 30; alarm-music"
alias binstall="bundle config set --local path 'vendor/bundle' && bundle install"
alias opt="optipng -o7 -zm1-9 -strip all -clobber"
eval "$(hub alias -s)"

export GOPATH=$HOME/dev/go
export ANDROID_HOME=$HOME/Android/Sdk
PATH=/home/ruben/.gem/ruby/2.6.0/bin:$ANDROID_HOME/platform-tools:$PATH

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

(cd $DIR; git diff-index --quiet HEAD -- ||
	(echo "dotfiles local changes detected at $DIR"; echo "pushd ~/dev/dotfiles; git add .; git commit; git push; popd"))
