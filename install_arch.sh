#!/bin/bash

set -e

CONFIG=$PWD

# YAY!
#sudo pacman -Su yay

# Basic programs
#yay -Su albert --needed
#sudo pacman -Su quassel-client --needed
#sudo pacman -Su cryptsetup --needed
#sudo pacman -Su simplescreenrecorder --needed
#yay -Su undistract-me-git --needed

# Redshift
#sudo pacman -Su redshift --needed
#mkdir ~/.config/redshift
#ln -s $CONFIG/redshift.conf ~/.config/redshift/redshift.conf

# IDEs and dev tools
#yay -Su clion --needed
#sudo pacman -Su code --needed
#yay -Su insomnia --needed

# Git
#git config --global user.email "rw@rubenwardy.com"
#git config --global user.name "rubenwardy"
#sudo pacman -Su hub --needed

# rubenimageupload
#mkdir -p ~/dev
#cd ~/dev
#git clone https://gitlab.com/rubenwardy/rubenimageupload
#cd rubenimageupload
#sudo pacman -Su xclip xsel python-pyperclip python-pysftp --needed
#sudo make install
#
# mgitstatus
#curl -s -o mgitstatus https://raw.githubusercontent.com/fboender/multi-git-status/master/mgitstatus
#chmod 755 mgitstatus
#sudo mv mgitstatus /usr/local/bin/

# Dev requirements
#sudo pacman -Su sfml --needed
#yay -Su sfgui --needed
#yay -Su thor --needed
#yay -Su enet --needed
#yay -Su irrlicht --needed
#sudo pacman -Su lua51 --needed
#sudo pacman -Su luajit --needed

echo "source $CONFIG/bashrc" >> ~/.bashrc

link {
	ln -s "${CONFIG}/$1" "$2"
}

#link xfce4/xfce4-keyboard-shortcuts.xml ~/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml
#link xfce4/xfce4-panel.xml ~/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml
#link xfce4/xfwm4.xml ~/.config/xfce4/xfconf/xfce-perchannel-xml/xfwm4.xml
#link xfce4/xsettings.xml ~/.config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml

mkdir -p ~/.local/bin
link m ~/.local/bin/m
